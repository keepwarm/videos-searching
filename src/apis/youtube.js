import axios from 'axios';

const KEY = 'AIzaSyA9hjTD1V8-TfbXl8HFB9cNvg8h6VHeeXs';


export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        type: 'video',
        maxResults: 5,
        key: KEY
    }
});


